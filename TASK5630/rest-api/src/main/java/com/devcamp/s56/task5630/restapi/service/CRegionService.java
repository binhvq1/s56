package com.devcamp.s56.task5630.restapi.service;

import java.util.ArrayList;
import java.util.List;

import com.devcamp.s56.task5630.restapi.model.CRegion;

public class CRegionService {
    private static List<CRegion> regionVietnam = new ArrayList<CRegion>();
    private static List<CRegion> regionUs = new ArrayList<CRegion>();
    private static List<CRegion> regionRussia = new ArrayList<CRegion>();

    static {
        CRegion dongnai = new CRegion("Đồng Nai",60);
        CRegion tphochiminh = new CRegion("Hồ CHí Minh",59);
        CRegion hanoi = new CRegion("Hà Nội",29);
        CRegion texas = new CRegion("Texas",29);
        CRegion florida = new CRegion("Florida",29);
        CRegion newyork = new CRegion("New York",29);
        CRegion moscow = new CRegion("Moscow",29);
        CRegion kaluga = new CRegion("Kaluga",29);
        CRegion sainpeter = new CRegion("Saint Peterburg",29);

        regionVietnam.add(dongnai);
        regionVietnam.add(tphochiminh);
        regionVietnam.add(hanoi);

        regionUs.add(texas);
        regionUs.add(florida);
        regionUs.add(newyork);

        regionRussia.add(moscow);
        regionRussia.add(kaluga);
        regionRussia.add(sainpeter);
    }

    public static List<CRegion> getRegionVietnam() {
        return regionVietnam;
    }

    public static void setRegionVietnam(List<CRegion> regionVietnam) {
        CRegionService.regionVietnam = regionVietnam;
    }

    public static List<CRegion> getRegionUs() {
        return regionUs;
    }

    public static void setRegionUs(List<CRegion> regionUs) {
        CRegionService.regionUs = regionUs;
    }

    public static List<CRegion> getRegionRussia() {
        return regionRussia;
    }

    public static void setRegionRussia(List<CRegion> regionRussia) {
        CRegionService.regionRussia = regionRussia;
    }
}
