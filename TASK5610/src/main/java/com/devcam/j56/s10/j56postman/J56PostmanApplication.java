package com.devcam.j56.s10.j56postman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J56PostmanApplication {

	public static void main(String[] args) {
		SpringApplication.run(J56PostmanApplication.class, args);
	}

}
