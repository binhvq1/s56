package com.devcamp.s50.task5620.restapi.model;

import java.time.LocalDate;

public class CDrink {
    private int id;
    private int day;
    private String maNuocUong;
    private String tenNuocUong;
    private long price;
    private LocalDate ngayTao;
    private LocalDate ngayCapNhat;

    public CDrink() {
        super();
    }

    public CDrink(int day,String maNuocUong,String tenNuocUong,long price,LocalDate ngayTao,LocalDate ngayCapNhat) {
        super();
        this.day = day;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    /*
     * get id
     * @return
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /*
     * get maNuocUong
     * @return
     */
    public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setId(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    /*
     * get tenNuocUong
     * @return
     */
    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    /*
     * get Price
     * @return
     */
    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    /*
     * get ngayTao
     * @return
     */
    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    /*
     * get ngayTao
     * @return
     */
    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

}
