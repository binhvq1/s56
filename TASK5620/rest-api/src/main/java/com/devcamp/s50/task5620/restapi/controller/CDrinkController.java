package com.devcamp.s50.task5620.restapi.controller;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5620.restapi.model.CDrink;

@RestController
public class CDrinkController {
    @CrossOrigin
	@GetMapping("/devcamp-drinks")
    // khai báo danh dách
    public ArrayList<CDrink> getDrink() {
        ArrayList<CDrink> listDrink = new ArrayList<CDrink>();
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        // khởi tạo các đối tượng
        CDrink tratac = new CDrink(1,"TRATAC","Trà Tắc",10000,today,today);
        CDrink cocacola = new CDrink(1,"COCACOLA","Cocacola",15000,today,today);
        CDrink pepsi = new CDrink(1,"PEPSI","Pepsi",15000,today,today);

        listDrink.add(tratac);
        listDrink.add(cocacola);
        listDrink.add(pepsi);

        return listDrink;

    } 
    
}
